// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import GroupIcon from '@material-ui/icons/Group';
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import Register from "@material-ui/icons/GroupAdd";
import Login from "@material-ui/icons/LockOpen";
import EventIcon from '@material-ui/icons/Event';

// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.jsx";
import UserProfile from "views/UserProfile/UserProfile.jsx";
import Typography from "views/Typography/Typography.jsx";
import Icons from "views/Icons/Icons.jsx";
import Maps from "views/Maps/Maps.jsx";
import NotificationsPage from "views/Notifications/Notifications.jsx";

// core components/views for Auth layout
import LoginPage from "views/Pages/LoginPage.jsx";
import RegisterPage from "views/Pages/RegisterPage.jsx";

// WORANA CRUD
import UserList from "views/User/UserList.jsx";
import UserCad from "views/User/UserCad.jsx";
import EventList from "views/Event/EventList.jsx";
import EventCad from "views/Event/EventCad.jsx";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin",
    menu: true,
    admin: true,
    provider: true,
    user: true
  },
  {
    path: "/profile",
    name: "Perfil",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Person,
    component: UserProfile,
    layout: "/admin",
    menu: true,
    admin: true,
    provider: true,
    user: true
  },
  {
    path: "/user/:id",
    name: "Usuário",
    rtlName: "قائمة الجدول",
    icon: GroupIcon,
    component: UserCad,
    layout: "/admin",
    menu: false,
    admin: true,
    provider: false,
    user: false
  },
  {
    path: "/user",
    name: "Usuários",
    rtlName: "قائمة الجدول",
    icon: GroupIcon,
    component: UserList,
    layout: "/admin",
    menu: true,
    admin: true,
    provider: false,
    user: false
  },
  {
    path: "/event/:id",
    name: "Evento",
    rtlName: "قائمة الجدول",
    icon: EventIcon,
    component: EventCad,
    layout: "/admin",
    menu: false,
    admin: true,
    provider: false,
    user: false
  },
  {
    path: "/event",
    name: "Eventos",
    rtlName: "قائمة الجدول",
    icon: EventIcon,
    component: EventList,
    layout: "/admin",
    menu: true,
    admin: true,
    provider: false,
    user: false
  },
  {
    path: "/typography",
    name: "Typography",
    rtlName: "طباعة",
    icon: LibraryBooks,
    component: Typography,
    layout: "/admin",
    menu: false,
    admin: true,
    provider: true,
    user: true
  },
  {
    path: "/icons",
    name: "Icons",
    rtlName: "الرموز",
    icon: BubbleChart,
    component: Icons,
    layout: "/admin",
    menu: false,
    admin: true,
    provider: true,
    user: true
  },
  {
    path: "/maps",
    name: "Maps",
    rtlName: "خرائط",
    icon: LocationOn,
    component: Maps,
    layout: "/admin",
    menu: true,
    admin: true,
    provider: true,
    user: true
  },
  {
    path: "/notifications",
    name: "Notifications",
    rtlName: "إخطارات",
    icon: Notifications,
    component: NotificationsPage,
    layout: "/admin",
    menu: false,
    admin: true,
    provider: true,
    user: true
  },
  {
    path: "/login-page",
    name: "Login Page",
    rtlName: "پشتیبانی از راست به چپ",
    icon: Login,
    component: LoginPage,
    layout: "/auth",
    menu: true,
    admin: true,
    provider: true,
    user: true
  },
  {
    path: "/register-page",
    name: "Register Page",
    rtlName: "پشتیبانی از راست به چپ",
    icon: Register,
    component: RegisterPage,
    layout: "/auth",
    menu: false,
    admin: true,
    provider: true,
    user: true
  }
];

export default dashboardRoutes;
