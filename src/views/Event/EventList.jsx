import React from "react";
import { Redirect } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";

// API WORANA
import api from "../../services/api.jsx";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  estiloCard: {
    marginTop: "80px"
  }
};

class EventList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      viewList: [],
      toDetail: undefined
    };
    this.editar = this.editar.bind(this);
    this.excluir = this.excluir.bind(this);
    this.cadastrar = this.cadastrar.bind(this);
  }
  cadastrar(){
    this.setState({
      toDetail: 0
    });
  }
  editar(id){
    this.setState({
      toDetail: id
    });
  }
  async excluir(id){
    if(id !== undefined){
      let deleteRequest;
      try {
        deleteRequest = await api.delete(`/events/${id}`);
      } catch ({ response }) {
        deleteRequest = response;
      }
      this.listar();
    }
  }
  async listar(){
    let listRequest;
    try {
      listRequest = await api.get("/events/");
    } catch ({ response }) {
      listRequest = response;
    }
    const { data: listRequestData } = listRequest;
    if (listRequestData.status === "success") {
      this.setState({
        viewList: listRequestData.data
      });
    }
  }
  componentDidMount() {
    this.listar();
  }
  render(){
    const { classes } = this.props;

    if (this.state.toDetail !== undefined) {
      return <Redirect to={`/admin/event/${this.state.toDetail}`}  />
    }

    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={4}></GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Button
            color="success" 
            round 
            fullWidth
            onClick={() => this.editar(0)}>
              Criar Novo Evento
          </Button>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}></GridItem>
        {this.state.viewList.map((prop, key) => {
          return (
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes.estiloCard} profile>
                <CardAvatar profile>
                  <a href="#pablo" onClick={e => e.preventDefault()}>
                    <img src={avatar} alt="..." />
                  </a>
                </CardAvatar>
                <CardBody profile>
                  <h6>{prop.user.name}</h6>
                  <h4>{prop.type}</h4>
                  <p>{prop.description}</p>
                  {/* <Tooltip
                      id="tooltip-top"
                      title="Editar"
                      placement="top"
                      classes={{ tooltip: classes.tooltip }}
                      >
                      <IconButton
                          onClick={() => this.editar(prop._id)}
                          aria-label="Edit"
                          className={classes.tableActionButton}
                      >
                          <Edit
                          className={
                              classes.tableActionButtonIcon + " " + classes.edit
                          }
                          />
                      </IconButton>
                  </Tooltip> */}
                  <Button color="primary" round>
                    Visualizar
                  </Button>
                  <Tooltip
                    id="tooltip-top-start"
                    title="Remover"
                    placement="top"
                    classes={{ tooltip: classes.tooltip }}
                    >
                    <IconButton
                        onClick={() => this.excluir(prop._id)}
                        aria-label="Close"
                        className={classes.tableActionButton}
                    >
                      <Close
                        className={
                            classes.tableActionButtonIcon + " " + classes.close
                        }
                      />
                    </IconButton>
                  </Tooltip>
                </CardBody>
              </Card>
            </GridItem>
          );
        })}
      </GridContainer>
    );
  }
}

export default withStyles(styles)(EventList);
