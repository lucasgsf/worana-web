import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

// API WORANA
import api from "../../services/api.jsx";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

class UserCad extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      data: {}
    };
    this.carregar(props.match.params.id);
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  async carregar(id){
    let getRequest;
    try {
      getRequest = await api.get(`/users/${id}`);
    } catch ({ response }) {
      getRequest = response;
    }
    const { data: getRequestData } = getRequest;
    if (getRequestData.status === "success") {
      this.setState({
        data: getRequestData.data[0],
      });
    }
  };
  handleInputChange(event) {
    console.log("a");
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    let newData = this.state.data;
    newData[name] = value;

    this.setState({
      data: newData
    });
  }
  salvar = async e => {
    e.preventDefault();

    const data = this.state.data;
    const fields = ["name", "email", "role"];
    const formElements = e.target.elements;

    const formValues = fields
      .map(field => ({
        [field]: formElements.namedItem(field).value
      }))
      .reduce((current, next) => ({ ...current, ...next }));

    let cadRequest;
    try {
      cadRequest = await api.patch(`/users/${data._id}`, { ...formValues });
    } catch ({ response }) {
      cadRequest = response;
    }
    const { data: cadRequestData } = cadRequest;
    if (cadRequestData.status === "success") {
    }

    this.setState({
      errors: (cadRequestData.status == "fail" || cadRequestData.status == "error") && cadRequestData.message
    });
  };
  render(){
    const { classes } = this.props;
    const { errors, data } = this.state;

    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <form onSubmit={this.salvar}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Editar Usuário</h4>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Nome"
                      id="name"
                      error={errors.name}
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        required: true,
                        defaultValue: "Nome",
                        value: data.name,
                        name: "name",
                        onChange: this.handleInputChange
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Email"
                      id="email"
                      error={errors.email}
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        required: true,
                        defaultValue: "Email",
                        value: data.email,
                        name: "email",
                        onChange: this.handleInputChange
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Função"
                      id="role"
                      error={errors.role}
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        required: true,
                        defaultValue: "Função",
                        value: data.role,
                        name: "role",
                        onChange: this.handleInputChange
                      }}
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button type="submit" color="primary">
                  Salvar
                </Button>
              </CardFooter>
            </Card>
          </form>
        </GridItem>
      </GridContainer>
    );
  }
}

export default withStyles(styles)(UserCad);
