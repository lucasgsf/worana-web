import React from "react";
import { Redirect } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/ActionTable.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";

// API WORANA
import api from "../../services/api.jsx";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      viewList: [],
      tableList: [],
      toDetail: undefined
    };
    this.editar = this.editar.bind(this);
    this.excluir = this.excluir.bind(this);
  }
  editar(index){
    let user = this.state.viewList[index];
    this.setState({
      toDetail: user._id
    });
  }
  async excluir(index){
    let user = this.state.viewList[index];
    if(user !== undefined){
      let deleteRequest;
      try {
        deleteRequest = await api.delete(`/users/${user._id}`);
      } catch ({ response }) {
        deleteRequest = response;
      }
      this.listar();
    }
  }
  async listar(){
    let listRequest;
    try {
      listRequest = await api.get("/users/");
    } catch ({ response }) {
      listRequest = response;
    }
    const { data: listRequestData } = listRequest;
    if (listRequestData.status === "success") {
      this.setState({
        viewList: listRequestData.data,
        tableList: listRequestData.data.map((c, i) => [`${c.name}`, `${c.email}`, `${c.role}` ])
      });
    }
  }
  componentDidMount() {
    this.listar();
  }
  render(){
    const { classes } = this.props;

    if (this.state.toDetail !== undefined) {
      return <Redirect to={`/admin/user/${this.state.toDetail}`}  />
    }

    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Usuários</h4>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["Nome", "Email", "Função", "Ações"]}
                tableData={this.state.tableList}
                funcaoEditar={this.editar}
                funcaoExcluir={this.excluir}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

export default withStyles(styles)(UserList);
