import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// @material-ui/icons
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";
// core components
import tableStyle from "assets/jss/material-dashboard-react/components/tableStyle.jsx";

function CustomTable({ ...props }) {
  const { classes, tableHead, tableData, tableHeaderColor, funcaoEditar, funcaoExcluir } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
            <TableRow>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {tableData.map((prop, key) => {
            return (
              <TableRow key={key}>
                {prop.map((prop, key) => {
                  return (
                    <TableCell className={classes.tableCell} key={key}>
                      {prop}
                    </TableCell>
                  );
                })}
                <TableCell className={classes.tableActions}>
                    {funcaoEditar !== undefined ? (
                        <Tooltip
                            id="tooltip-top"
                            title="Editar"
                            placement="top"
                            classes={{ tooltip: classes.tooltip }}
                            >
                            <IconButton
                                onClick={() => funcaoEditar(key)}
                                aria-label="Edit"
                                className={classes.tableActionButton}
                            >
                                <Edit
                                className={
                                    classes.tableActionButtonIcon + " " + classes.edit
                                }
                                />
                            </IconButton>
                        </Tooltip>
                    ) : null}
                    {funcaoExcluir !== undefined ? (
                        <Tooltip
                            id="tooltip-top-start"
                            title="Remover"
                            placement="top"
                            classes={{ tooltip: classes.tooltip }}
                            >
                            <IconButton
                                onClick={() => funcaoExcluir(key)}
                                aria-label="Close"
                                className={classes.tableActionButton}
                            >
                                <Close
                                className={
                                    classes.tableActionButtonIcon + " " + classes.close
                                }
                                />
                            </IconButton>
                        </Tooltip>
                    ) : null}
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </div>
  );
}

CustomTable.defaultProps = {
  tableHeaderColor: "gray"
};

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  funcaoEditar: PropTypes.func,
  funcaoExcluir: PropTypes.func
};

export default withStyles(tableStyle)(CustomTable);
